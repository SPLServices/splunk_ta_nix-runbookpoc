.PHONY=help package clean
.DEFAULT=help
# apps are ordered based on dependency
#DEPENDENCY_APPS=pas_hr_info pas_simulated_application_addon pas_simulated_database_addon pas_simulated_files_addon pas_simulated_keycard_addon
DEPENDENCY_APPS=
#OPTIONAL_GIT_DEPS=eventgen splunk-add-on-google-drive splunk-add-on-jira-alerts
OPTIONAL_GIT_DEPS=

APPS_DIR=src

BITBUCKET_MAJOR_NUMBER=$(shell grep BITBUCKET_MAJOR_NUMBER version.ini | cut -d = -f 2 | tr -d '[:space:]')
BITBUCKET_MINOR_NUMBER=$(shell grep BITBUCKET_MINOR_NUMBER version.ini | cut -d = -f 2 | tr -d '[:space:]')
BITBUCKET_PATCH_NUMBER=$(shell grep BITBUCKET_PATCH_NUMBER version.ini | cut -d = -f 2 | tr -d '[:space:]')
BITBUCKET_BUILD_NUMBER=$(shell grep BITBUCKET_BUILD_NUMBER version.ini | cut -d = -f 2 | tr -d '[:space:]')
BITBUCKET_THUMBPRINT?=local
MAIN_APP=$(shell grep MAIN_APP version.ini | cut -d = -f 2 | tr -d '[:space:]')
DEPLOYMENT_APPS = $(shell grep DEPLOYMENT_APPS version.ini | cut -d = -f 2 )

APP_SOURCE_DIRS=$(foreach app,$(DEPENDENCY_APPS),$(APPS_DIR)/$(app))
OUT_DIR=out
PACKAGES_DIR=$(OUT_DIR)/work/packages
STANDALONE_BUILD=$(OUT_DIR)/work/standalone
PARTITIONED_DIR=$(OUT_DIR)/release/partitioned
ONE_CLICK_DIR=$(OUT_DIR)/release/oneclick
STANDALONE_DIR=$(OUT_DIR)/release/standalone
OPTIONAL_DEPENDENCY_DIR=$(OUT_DIR)/work/optional_dependencies
MAIN_APP_VERSION=$(eval grep version $(APPS_DIR)/$(MAIN_APP)/default/app.conf | cut -d = -f 2)
MAIN_APP_PACKAGE=$(MAIN_APP)-$(MAIN_APP_VERSION).tar.gz
STANDALONE_DEP_DIR=$(STANDALONE_BUILD)/$(MAIN_APP)/appserver/addons



help: ## Show this help message.
	@echo 'usage: make [target] ...'
	@echo
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' $(MAKEFILE_LIST) | column -t -c 2 -s ':#' | sed 's/^/  /'

clean: ## Remove artifacts
	@rm -r $(OUT_DIR)

$(PACKAGES_DIR):
	@mkdir -p $(PACKAGES_DIR)

$(PARTITIONED_DIR):
	@mkdir -p $(PARTITIONED_DIR)

$(ONE_CLICK_DIR):
	@mkdir -p $(ONE_CLICK_DIR)

$(STANDALONE_BUILD):
	@mkdir -p $(STANDALONE_BUILD)

$(STANDALONE_DIR):
	@mkdir -p $(STANDALONE_DIR)

update_version: ## Update version in each app
update_version:
	@mkdir -p $(OUT_DIR)
	@for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do \
		echo Updating Version for $$i ;\
		crudini --set src/$$i/default/app.conf launcher version $(BITBUCKET_MAJOR_NUMBER).$(BITBUCKET_MINOR_NUMBER).$(BITBUCKET_PATCH_NUMBER);\
		crudini --set src/$$i/default/app.conf install build $(BITBUCKET_BUILD_NUMBER);\
		slim generate-manifest --update -o $(OUT_DIR)/$$i.manifest src/$$i ;\
		cp $(OUT_DIR)/$$i.manifest src/$$i/app.manifest ;\
	done

package: ## Package each app
package:
	@mkdir -p $(OUT_DIR)
	@mkdir -p $(PACKAGES_DIR)
	@for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do \
		echo Packaging for $$i ;\
		slim package -o $(PACKAGES_DIR) src/$$i;\
	done

initappmanifest: ## Initialize a basic app.conf and app.manifest
initappmanifest:
	@for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do \
		crudini --set src/$$i/default/app.conf launcher version $(BITBUCKET_MAJOR_NUMBER).$(BITBUCKET_MINOR_NUMBER).$(BITBUCKET_PATCH_NUMBER).$(BITBUCKET_BUILD_NUMBER)-$(BITBUCKET_THUMBPRINT);\
		crudini --set src/$$i/default/app.conf install build $(BITBUCKET_BUILD_NUMBER);\
		crudini --set src/$$i/default/app.conf package id $$i;\
		if [ "$MAIN_APP" = "$$i" ]; then \
			crudini --set src/$$i/default/app.conf package check_for_updates true; \
		fi ; \
		if ! grep --quiet is_visible src/$$i/default/app.conf; then \
			crudini --set src/$$i/default/app.conf ui is_visible false;\
		fi ;\
		if [ ! -e src/$$i/app.manifest ]; then \
			slim generate-manifest -o src/$$i/app.manifest src/$$i;\
		fi \
	done

partition: ## Partition the primary app
partition:
		@mkdir -p $(PARTITIONED_DIR)
		slim partition -o $(PARTITIONED_DIR) $(shell ls -1 out/work/packages/$(MAIN_APP)-*.gz)

devlink: ## Link apps into a Splunk installation
devlink:
	@if [ -d $(SPLUNK_HOME)/etc/apps ]; then \
		for i in $(realpath $(APP_SOURCE_DIRS)); do \
			ln -s $$i $(SPLUNK_HOME)/etc/apps; \
		done \
	else \
		echo "Could not find Splunk app home at $(SPLUNK_HOME)/etc/apps"; \
		exit 1; \
	fi


standalone_package: ## Build package for local install
standalone_package: $(STANDALONE_DIR) $(STANDALONE_BUILD) optional_dependencies
	@cp -r $(APPS_DIR)/$(MAIN_APP) $(STANDALONE_BUILD)
	@mkdir -p $(STANDALONE_DEP_DIR)
	@for a in $(realpath $(APP_SOURCE_DIRS)); do \
	  cp -r $$a $(STANDALONE_DEP_DIR); \
	done
	@for dep in $(OPTIONAL_GIT_DEPS); do \
	  cp -r $(OPTIONAL_DEPENDENCY_DIR)/$$a $(STANDALONE_DEP_DIR); \
	done
	tar -cz -C $(STANDALONE_BUILD) -f ${PWD}/$(STANDALONE_DIR)/$(MAIN_APP_PACKAGE) .

package_all: ## Build packages for standalone, cluster (partitioned), and oneclick cloud
package_all: standalone_package package_oneclick partition

optional_dependencies:
	@mkdir -p $(OPTIONAL_DEPENDENCY_DIR)
	@for i in $(OPTIONAL_GIT_DEPS); do \
		if [ ! -d $(OPTIONAL_DEPENDENCY_DIR)/$$i ]; then \
			echo $$i; \
			git clone --depth 1 https://github.com/splunk/$$i.git $(OPTIONAL_DEPENDENCY_DIR)/$$i; \
			rm -rf $(OPTIONAL_DEPENDENCY_DIR)/$$i/.git; \
		fi \
	done

run_in_docker: ## Build a docker image and run it with the app installed
run_in_docker: optional_dependencies
	@echo "Building image"
	$(eval $@_IMAGE := $(shell docker build -q .))
	@echo "Starting container"
	$(eval $@_CONTAINER := $(shell docker run --env SPLUNK_START_ARGS="--accept-license" -p 8000:8000 -d --rm $($@_IMAGE)))
	$(eval $@_OS := $(shell uname))
	@echo "Launching Splunk "
	@sleep 10
	@echo "Launching browser"
	@case $($@_OS) in \
		"Darwin") open http://localhost:8000 ;; \
		*) which xdg-open > /dev/null && xdg-open http://localhost:8000 || echo "Splunk is ready at http://localhost:8000" ;; \
	esac
	@echo "Press Ctrl-C to stop and remove the container"
	@docker attach $($@_CONTAINER) || echo "Done."
