# About This Addon

The Splunk Technology Add-on for Unix and Linux works with the Splunk App for Unix and Linux to provide rapid insights and operational visibility into large-scale Unix and Linux environments. With its new pre-packaged alerting capability, flexible service-based hosts grouping, and easy management of many data sources, it arms administrators with a powerful ability to quickly identify performance and capacity bottlenecks and outliers in Unix and Linux environments.

## Derivative Work

This repository is derived from the original release available and documented below.

* [Splunk Base Public release][upstream_splunkbase]
* [Splunk Docs][upstream_docs]

## Content Releases
Updated content is available [here] [repo] packages are available using the "downloads" link on the left hand navigation bar


## Runbooks

Runbooks are procedural guidance to implement data collection for the technology covering typical implementation goals. Runbooks created should consider:


	* Monitor system events for IT Operations
	* Monitor system events for Security Operations
	* (optional) collect performance metrics

	
	
| Runbook | Description | link |
|-------------|----------|--------|
| Out of Box (defaults)| Collect as much possibe applying inputs regardless of operating system | [here](runbooks/rb-oob.md) |
| Success Enablement Content (SecKit) | Selective collection by objective and platform | [here](runbooks/rb-seckit.md) | 









[upstream_splunkbase]: https://splunkbase.splunk.com/app/833/ "Supported Release"
[upstream_docs]: http://docs.splunk.com/Documentation/UnixAddOn/latest/User/AbouttheSplunkAdd-onforUnixandLinux "Supported Docs"
[repo]: https://bitbucket.org/SPLServices/splunk_ta_nix-runbookpoc "Services Repository Splunk_TA_nix"
