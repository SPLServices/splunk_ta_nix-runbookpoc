# Using OOB Configuration

## Requirements

* Supported Operating System
* Splunk Enterprise or Splunk Cloud

## Splunk Cloud
1. Open a TA deployment request for Splunk_TA_nix
2. Utilize Services or Admin on Demand to patch TA
3. Define each index [indexes.conf](../../src/SecKit_splunk_index_2_nix_home/default)

## Splunk Enterprise Cluster Master

1. Download latest `Splunk_TA_nix-*_indexers.tar.gz`
2. Splunk Cluster Master unzip to master-apps
3. Deploy one of
	4. `SecKit_splunk_index_2_nix_home-5.2.4.tar.gz`
	5. `SecKit_splunk_index_2_nix_vol-5.2.4.tar.gz`

## Splunk Enterprise Indexers and Intemediate Forwarders
1. Download latest `Splunk_TA_nix-*_indexers.tar.gz`
2. Login (sudo) to the splunk user `sudo su - splunk`
3. Install the app `splunk app install <filename>`
4. Define custom indexes as neded
4. Restart splunk `systemctl restart splunk`
5. Deploy one of
	4. `SecKit_splunk_index_2_nix_home-5.2.4.tar.gz`
	5. `SecKit_splunk_index_2_nix_vol-5.2.4.tar.gz`

## Splunk Enterprise Search Head
1. Download latest `Splunk_TA_nix-5.2.4-_search_heads.tar.gz`
2. Install the app `splunk app install <filename>`

## Source Operating Sytem Configuration

### Linux

Pre-implementation Procedure

1. Ensure each linux host is correctly named using the proper fqdn of the host, using the correct procedure for your operating system resolve any occurrences of incorrect host name. A reboot is required if the host name is altered `hostname -f`
2. Log Rotate 
	1. update syslog rotation to rotate daily and retain no more than 7 days locally
	1. Conditional: If Splunk is not running as root ensure the splunk ID can access critical files

``` bash
sudo /usr/bin/setfacl -m "u:splunk:r-x" /var/log
sudo /usr/bin/setfacl -m "u:splunk:r--" /var/log/*
sudo /usr/bin/setfacl -m d:user:splunk:r /var/log
sudo /usr/bin/setfacl -m "u:splunk:r-x" /etc
sudo /usr/bin/setfacl -m "u:splunk:r--" /etc/*
```

### AIX

Pre-implementation Procedure

1. Ensure each linux host is correctly named using the proper fqdn of the host, using the correct procedure for your operating system resolve any occurrences of incorrect host name. A reboot is required if the host name is altered `hostname -f`
1. Log Rotate 
	1. update syslog rotation to rotate daily and retain no more than 7 days locally
1. Adjust ulimits, default AIX ulimits are too low to expect forwarder to run reliably.  Unless customer has higher limits already in place, use these as a minimum:

```
chsec -f /etc/security/limits -s default -a rss=2097152
chsec -f /etc/security/limits -s default -a fsize=-1
chsec -f /etc/security/limits -s default -a data=2097152

```

### Sun Unix

Pre-implementation Procedure

1. Ensure each linux host is correctly named using the proper fqdn of the host, using the correct procedure for your operating system resolve any occurrences of incorrect host name. A reboot is required if the host name is altered `hostname -f`
1. Log Rotate 
	1. update syslog rotation to rotate daily and retain no more than 7 days locally


## Splunk Deploymment Server
1. Download and Deploy `Splunk_TA_nix_SecKit_DeploymentServer-5.2.4.tar.gz`
2. Download and unzip to deployment-apps
	3. `Splunk_TA_nix-*.tar.gz`
	4. 	`Splunk_TA_nix_SecKit_*.tar.gz`
	


## Post Install Validation

1. Run the following search or derivative based on deployment scope 

```| tstats count where index=osnix* by host```
